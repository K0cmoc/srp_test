<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_export".
 *
 * @property int $id
 * @property int $client_id
 * @property string|null $name
 * @property int|null $total_cost

 */
class ClientExport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_export';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total_cost'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'total_cost' => 'Total Cost',
        ];
    }

    public function getClients()
    {
        return $this->hasMany(Client::className(), ['id' => 'client_id']);
    }
}
