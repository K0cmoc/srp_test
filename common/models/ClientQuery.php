<?php


namespace common\models;


use yii\db\ActiveQuery;

class ClientQuery extends ActiveQuery
{
    public function dateOfBirth($date)
    {
        return $this->andWhere(['date_of_birth' => $date]);
    }
    public function notId($id)
    {
        return $this->andWhere(['!=', 'id', $id]);
    }
}