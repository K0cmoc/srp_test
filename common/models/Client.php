<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string|null $date_of_birth
 * @property Order[] orders
 * @property ClientExport clientExport
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'required'],
            [['date_of_birth'], 'safe'],
            ['date_of_birth', 'date', 'format' => 'php:Y-m-d', 'message' => 'Date of birth must be in format YYYY-mm-dd'],
            [['email', 'name'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'date_of_birth' => 'Date Of Birth',
        ];
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['client_id' => 'id']);
    }

    public function getClientExport()
    {
        return $this->hasOne(ClientExport::className(), ['client_id' => 'id']);
    }

    public static function find()
    {
        return new ClientQuery(static::class);
    }
}
