<?php


namespace common\models;


use yii\db\ActiveRecord;

/**
 * Class OrderProduct
 * @package common\models
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 */
class OrderProduct extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%order_product}}';
    }
}