<?php
namespace console\controllers;


use common\models\Client;
use common\models\Order;
use common\models\OrderProduct;
use common\models\Product;
use console\services\fake\CreateRandomClientService;
use console\services\fake\CreateRandomOrderService;
use console\services\fake\CreateRandomProductService;
use yii\console\Controller;

class FakeController extends Controller
{
    public function actionIndex()
    {
        echo 'yii fake/create number -- default value 5' . PHP_EOL;
        echo 'yii fake/delete' . PHP_EOL;
    }

    public function actionCreate($number =5)
    {
        for ($i=0; $i<$number; $i++) {
            $createRandomClient = new CreateRandomClientService();
            $client = new Client();
            $createRandomClient->createRandomClient($client);

            $createRandomProduct = new CreateRandomProductService();
            $product = new Product();
            $createRandomProduct->createRandomProduct($product);

            $createRandomOrder = new CreateRandomOrderService();
            $order = new Order();
            $createRandomOrder->createRandomOrder($order);
        }
    }

    public function actionDelete()
    {
        Client::deleteAll();
        Product::deleteAll();
        Order::deleteAll();
        OrderProduct::deleteAll();
    }
}