<?php


namespace console\controllers;


use common\models\Client;
use common\models\ClientExport;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use yii\console\Controller;

class ReportController extends Controller
{
    public function actionIndex()
    {
        echo 'yii report/report filePath --default value "backend/web"' . PHP_EOL;
    }

    public function actionReport($filePath = 'backend/web')
    {
        //Creating data with total_cost
        $arrayForExport = [];
        $clients = Client::find()->with('orders')->all();

        /** @var Client $client */
        foreach ($clients as $client) {
            $clientExportRow = [];
            if(!$client->clientExport) {
                $clientExport = new ClientExport();
            } else {
                $clientExport = $client->clientExport;
            }
            if($clientExport->name != $client->name) {
                $clientExportRow []= $client->name;
            } else {
                $clientExportRow [] = null;
            }
            $totalCost = 0;
            foreach ($client->orders as $order) {
                $totalCost += $order->order_cost;
            }
            if($clientExport->total_cost != $totalCost) {
                $clientExportRow [] = $totalCost;
            } else {
                $clientExportRow [] = null;
            }
            $clientExport->name = $client->name;
            $clientExport->total_cost = $totalCost;
            $clientExport->client_id = $client->id;
            $clientExport->save(false);
            $arrayForExport[] = $clientExportRow;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($arrayForExport, ' ');

        $writer = new Csv($spreadsheet);
        $writer->setUseBOM(true);
        $writer->setEnclosure('');
        $writer->setLineEnding(",\r\n");
        $writer->save($filePath . '/report.csv');
    }
}