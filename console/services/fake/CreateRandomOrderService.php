<?php


namespace console\services\fake;


use common\models\Client;
use common\models\Order;
use common\models\OrderProduct;
use common\models\Product;

class CreateRandomOrderService
{
    public function createRandomOrder(Order $order)
    {
        $this->addProductsToOrder($order);
        return $this->addOrderCost($order);
    }

    private function getRandomClient()
    {
        $arrayOfId = [];
        /** @var Client $client */
        foreach (Client::find()->all() as $client) {
            $arrayOfId[] = $client->id;
        }
        $randId = array_rand($arrayOfId);
        $clientId = Client::findOne($arrayOfId[$randId])->id;
        return $clientId;
    }

    private function getRandomProducts()
    {
        $arrayOfId = [];
        /** @var Product $product */
        foreach (Product::find()->all() as $product) {
            $arrayOfId[] = $product->id;
        }
        $randId = array_rand($arrayOfId);
        $productId = Product::findOne($arrayOfId[$randId])->id;
        return $productId;
    }

    private function addOrderId($model)
    {
        $model->client_id = $this->getRandomClient();
        $model->save(false);
        return $model->id;
    }

    private function addProductsToOrder($model)
    {
        for ($i=0; $i<random_int(1, 5); $i++) {
            $orderProduct = new OrderProduct();
            $orderProduct->order_id = $this->addOrderId($model);
            $orderProduct->product_id = $this->getRandomProducts();
            $orderProduct->save(false);
        }
        return $model;
    }

    private function addOrderCost($model)
    {
        $order = Order::findOne($model->id);
        $products = $order->products;
        $orderCost = 0;
        foreach ($products as $product) {
            $orderCost += $product->price;
        }
        $model->order_cost = $orderCost;
        $model->save(false);
        return $model;
    }
}