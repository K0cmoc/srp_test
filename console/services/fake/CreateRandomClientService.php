<?php
namespace console\services\fake;

use common\models\Client;
use Faker;

class CreateRandomClientService
{
    public function createRandomClient(Client $client)
    {
        $faker = Faker\Factory::create();
        $client->email = $faker->unique()->email;
        $client->name = $faker->name();
        $client->date_of_birth = $faker->date();
        $client->save(false);
        return $client;
    }
}