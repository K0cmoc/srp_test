<?php


namespace console\services\fake;

use common\models\Product;
use Faker;

class CreateRandomProductService
{
    public function createRandomProduct(Product $product)
    {
        $faker = Faker\Factory::create();
        $product->name = $faker->word();
        $product->description = $faker->text();
        $product->price = $faker->randomNumber();
        $product->save(false);
        return $product;
    }
}