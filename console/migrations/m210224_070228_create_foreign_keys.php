<?php

use yii\db\Migration;

/**
 * Class m210224_070228_create_foreign_keys
 */
class m210224_070228_create_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk-order-client_id', '{{%order}}', 'client_id', '{{%client}}', 'id', 'CASCADE');
        $this->addForeignKey('fk-order_product-order_id', '{{%order_product}}', 'order_id', '{{%order}}', 'id', 'CASCADE');
        $this->addForeignKey('fk-order_product-product_id', '{{%order_product}}', 'product_id', '{{%product}}', 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210224_070228_create_foreign_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210224_070228_create_foreign_keys cannot be reverted.\n";

        return false;
    }
    */
}
