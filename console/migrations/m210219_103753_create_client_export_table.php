<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_export}}`.
 */
class m210219_103753_create_client_export_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_export}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'name' => $this->string(),
            'total_cost' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_export}}');
    }
}
