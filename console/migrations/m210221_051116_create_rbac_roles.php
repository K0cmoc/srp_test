<?php

use yii\db\Migration;

/**
 * Class m210221_051116_create_rbac_roles
 */
class m210221_051116_create_rbac_roles extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->createRole('superAdmin');
        $role->description = 'SuperAdmin';
        $auth->add($role);
        $role = $auth->createRole('user');
        $role->description = 'User';
        $auth->add($role);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210221_051116_create_rbac_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210221_051116_create_rbac_roles cannot be reverted.\n";

        return false;
    }
    */
}
