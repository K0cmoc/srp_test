<?php

use yii\db\Migration;

/**
 * Class m210221_051910_create_users
 */
class m210221_051910_create_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('superAdmin');
        $this->insert('{{%user}}',
            [
                'username' => 'SuperAdmin',
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('qwerty'),
                'email' => 'superadmin@email.com',
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        $adminID = Yii::$app->db->getLastInsertID();
        $auth->assign($adminRole, $adminID);

        $userRole = $auth->getRole('user');
        $this->insert('{{%user}}',
            [
                'username' => 'User',
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('qazwsx'),
                'email' => 'user@email.com',
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        $userID = Yii::$app->db->getLastInsertID();
        $auth->assign($userRole, $userID);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210221_051910_create_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210221_051910_create_users cannot be reverted.\n";

        return false;
    }
    */
}
