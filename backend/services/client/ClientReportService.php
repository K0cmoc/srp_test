<?php


namespace backend\services\client;


use common\models\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class ClientReportService
{
    public function report()
    {
        $reportData = [];
        $clients = Client::find()->with('orders')->all();
        /** @var Client $client */
        foreach ($clients as $client){
            $reportRow = [$client->name];
            foreach ($client->orders as $order){
                $reportRow[] = 'Заказ' . $order->id . ' - ' . $order->order_cost . 'руб';
            }
            $reportData[] = $reportRow;
        }
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($reportData, NULL, 'A1');

        $writer = new Xls($spreadsheet);
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="report.xls"');
        $writer->save('php://output');
    }
}