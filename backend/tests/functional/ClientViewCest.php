<?php


namespace backend\tests\functional;


use backend\tests\FunctionalTester;
use common\models\Client;
use common\models\User;
use Faker;
use Yii;

class ClientViewCest
{
    /**
     * @var FunctionalTester
     */
    private $tester;

    public function _before(FunctionalTester $I)
    {
        $this->tester = $I;
    }

    public function successViewTest()
    {
        User::deleteAll();
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('superAdmin');
        $userId = $this->tester->haveRecord(User::class, [
            'username' => 'SuperAdmin',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('qwerty'),
            'email' => 'superadmin@email.com',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $auth->assign($adminRole, User::findOne($userId)->id);

        $this->tester->amOnPage('/site/login');
        $this->tester->fillField('#loginform-username', 'SuperAdmin');
        $this->tester->fillField('#loginform-password', 'qwerty');
        $this->tester->submitForm('form', []);

        $email = Faker\Factory::create()->email;
        $name = Faker\Factory::create()->name;
        $dateOfBirth = Faker\Factory::create()->unique()->date();

        $id = $this->tester->haveRecord(Client::class, [
            'email' => $email,
            'name' => $name,
            'date_of_birth' => $dateOfBirth,
        ]);

        $name1 = Faker\Factory::create()->name;
        $this->tester->haveRecord(Client::class, [
            'email' => Faker\Factory::create()->email,
            'name' => $name1,
            'date_of_birth' => $dateOfBirth,
        ]);

        $name2 = Faker\Factory::create()->name;
        $this->tester->haveRecord(Client::class, [
            'email' => Faker\Factory::create()->email,
            'name' => $name2,
            'date_of_birth' => $dateOfBirth,
        ]);

        $dateOfBirth2 = Faker\Factory::create()->unique()->date();
        $name3 = Faker\Factory::create()->name;
        $this->tester->haveRecord(Client::class, [
            'email' => Faker\Factory::create()->email,
            'name' => $name3,
            'date_of_birth' => $dateOfBirth2,
        ]);

        $view = Client::findOne($id);

        $this->tester->amOnPage(['/client/view', 'id' => $view->id]);

        $this->tester->see("{$dateOfBirth} ({$name1}, {$name2})");
        $this->tester->cantSee($name3);
    }

    public function successNotInViewTest()
    {
        User::deleteAll();
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('superAdmin');
        $userId = $this->tester->haveRecord(User::class, [
            'username' => 'SuperAdmin',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('qwerty'),
            'email' => 'superadmin@email.com',
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $auth->assign($adminRole, User::findOne($userId)->id);

        $this->tester->amOnPage('/site/login');
        $this->tester->fillField('#loginform-username', 'SuperAdmin');
        $this->tester->fillField('#loginform-password', 'qwerty');
        $this->tester->submitForm('form', []);

        $email = Faker\Factory::create()->email;
        $name = Faker\Factory::create()->name;
        $dateOfBirth = Faker\Factory::create()->unique()->date();

        $id = $this->tester->haveRecord(Client::class, [
            'email' => $email,
            'name' => $name,
            'date_of_birth' => $dateOfBirth,
        ]);

        $view = Client::findOne($id);

        $this->tester->amOnPage(['/client/view', 'id' => $view->id]);

        $this->tester->cantSee("{$dateOfBirth} (");
    }
}