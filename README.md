###dependencies:###

- docker

###installation:###

1) add `127.0.0.1 frontend.local` and `127.0.0.1 backend.local` to your hosts file


2) rename docker-compose.override.yml.sample into docker-compose.override.yml

this file contains services configuration for ports
so you can run your application in browser on address 
`http://localhost:8000`
also you can add `db` and `db_test` config to use databases in your SQL-client
 
3) docker-compose build

4) `docker-compose up -d`
   `docker-compose exec php bash`
   `composer install`
   `php init` - choose 0
   `php yii migrate`

to fill the tables with the fake data, run in docker container next command
```php yii fake/create number``` 
this command will create "number" records in tables Client, Product, Record
the default value number = 5

to create csv with clients and total_cost, run in docker container next command
```php yii report/report $filePath```
the default value $filePath= '/backend/web'

to run tests, run in docker container next command  
```php yii_test migrate --interactive=0 && vendor/bin/codecept run```
